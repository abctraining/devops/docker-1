# Docker 1: Essentials Overview

### Course details
Docker is the world's most popular container storage solution. This course covers everything you need to know to install and configure Docker on Mac, Windows, and Linux, as well as cloud-based environments such as Amazon Web Services. It also helps Docker Certified Associate (DCA) candidates prepare for the certification exam. 
This course shows how to install Docker, set up a repository, configure logging, manage users, understand namespaces, and protect your data. It also covers features that ship with Docker Enterprise, including the Universal Control Plane and Trusted Registry. Plus, get important tips you'll need to prepare for the Docker Certified Associate (DCA) exam.

### Course Outline

    • Introduction
        1. Course Introduction
        2. About Exercise Architecture

    • Part 1: Intro to Containers
        1. Application Delivery history
            a. The Server as the unit of delivery
                i. Bare Metal
                ii. Virtual Machines
                iii. OS Level Virtual Machines
                iv. Automation
            b. The Application container as the unit of delivery
                i. Docker Containers
                ii. Other Application Container Engines
        2. Modern Service Architecture
            a. Cattle vs Pets
            b. Intro to Microservices
            c. The 12 factor application
        3. Hands-on Exercise #1

    • Part 2: Setup Docker
        1. Docker components
        2. Docker Installation
            a. Major Linux Distributions
                i. CentOS
                ii. Ubuntu
            b. Docker Desktop
                i. Windows
                ii. Mac
            c. In production options
                i. General Purpose Distributions
                ii. Container Optimized Distributions
        3. Hands-on Exercise #2

    • Part 3: Docker container process
        1. Isolation with Linux Namespaces
        2. Resource limiting with Linux cgroups
        3. Docker Networking Overview
            a. Bridge
            b. Host
            c. MACVLAN
            d. none
        4. Hands-on Exercise #3

    • Part 4: Docker images
        1. Build, Ship, Run
        2. Docker Image storage
            a. Union FS
        3. The Dockerfile basics
        4. Docker image management
        5. Hands-on Exercise #4

    • Part 5: Running Docker Containers
        1. Container life-cycle
            a. Container State
            b. Deployment
            c. Start and Stop
            d. Remove
        2. Accessing Containers
            a. Interactive shell
            b. Container logs
            c. Publish ports
        3. Docker Volumes
            a. Docker managed volumes
            b. Bind mounted volumes
        4. Multi Host
            a. Image Registry
            b. Basic multi host storage options
            c. Need for an Orchestrator
        5. Docker Enterprise
            a. Universal Control Plane (UCP)
            b. Docker Trusted Registry (DTR)
        6. Hands-on Exercise #5

    • Part 6: Application container security
        1. Data Security
        2. Privileged user access
            a. Dropping process privileges
        3. Vetting images
        4. Hands-on Exercise #6

    • Part 7: The Docker Certified Associate (DCA) exam
        1. What to expect
        2. How to prepare for the exam

    • Summary and Wrap-up
