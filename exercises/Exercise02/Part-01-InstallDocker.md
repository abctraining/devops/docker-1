# Exercise #2 : Part 1
## Install Docker

### Intro

In this section we will walk through the installation of Docker on your lab instance.  As mentioned before your lab instance is running on Ubuntu 18.04.  We will be installing Docker from the Docker Inc. maintainer package repositories.

### Install Docker

More information regarding this installation can be found on [Dockers documentation](https://docs.docker.com/engine/install/ubuntu/) site.

First let's verify that the local package database is up-to-database

##### Install Docker dependancies

~~~shell
sudo apt update
~~~

Next we will install the dependency packages need by Docker.

~~~shell
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
~~~

##### Add Docker Inc GPG key

The Docker package is cryptographically signed by Docker Inc.  We need to install and trust their key to allow the package to be installed.

~~~shell
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
~~~

We can verify that key with this command.

~~~shell
sudo apt-key fingerprint 0EBFCD88
~~~

The returned result should look like this.

~~~
pub   rsa4096 2017-02-22 [SCEA]
      9DC8 5822 9FC7 DD38 854A  E2D8 8D81 803C 0EBF CD88
uid           [ unknown] Docker Release (CE deb) <docker@docker.com>
sub   rsa4096 2017-02-22 [S]
~~~

##### Add the Docker Inc repository

We can add the needed package repository to the system with this command

~~~shell
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
~~~

##### Install the docker-ce package

Since we are not using the Ubuntu maintained package for Docker we need to use `docker-ce` as the package name.  Docker Inc uses the `docker-ce` name to indicate the community edition of their maintained Docker package.  Using that name also prevents them from conflicting with the distribution maintained Docker Package

Since we have added a new repo to the system we need to first update our local package databases to include the new repo.

~~~shell
sudo apt update
~~~

Notice that `download.docker.com` is now one of the hosts that are providing a package repository.

Now we can do the install.

~~~shell
sudo apt-get install -y docker-ce docker-ce-cli containerd.io
~~~

#####  Allow the `student` user access to the docker daemon

Docker listens on a file socket that is owned by the `root` user on the system.  To access this socket without escalated privileges we need to add the user to the `docker` group.

~~~shell
sudo usermod -aG docker student
~~~

Groups are only read when the shell is started so you will need to logout and back into your lab instace for that change to take effect.

Once you have reconnected to the lab intance verify that you can communicate with the docker daemon by checking the version without using sudo.

~~~shell
docker version
~~~

If everything went well you should see the version of the docker client and the docker daemon installed on your lab instance.

### (Optionally) Installing Docker on your desktop

Docker requires a linux kernel so it can not run directly on a Windows or Mac machine.  Docker Inc does provide a application they call Docker Desktop to allow for that installation.  Docker Desktop creates a Virtual Machine running Linux to run Docker on.  It also links the docker client on your desktop to the docker daemon running on the managed Virtual Machine.

If you are interested in installing on your own machine more information can be found at.

[https://docs.docker.com/desktop/](https://docs.docker.com/desktop/)

###

You now have docker installed and ready to go on your lab instance.

This concludes part 1 of Hands-on Exercise #2.  Continue on to Part 2 next.

[Part 2: Docker Hello World](Part-02-DockerHelloWorld.md)
