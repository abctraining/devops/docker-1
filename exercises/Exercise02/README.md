# Docker 1: Essentials
# Hands-on Exercise #1

### Objective

Install Docker on Lab instance.  Run First Docker Containers.  Review Docker CLI Basics.

### Parts

[Part 1: Install Docker](Part-01-InstallDocker.md)

[Part 2: Docker Hello World](Part-02-DockerHelloWorld.md)

[Part 3: Docker command basics](Part-03-DockerCommandBasics.md)
