# Exercise #2 : Part 3
## Docker command basics

### Intro

In this section we will go over some basic info regarding the `docker` cli command.  The `docker` command is our primary interface to manage container and images on our Linux host.

#### Command syntax

All docker commands follow a standard syntax.  There are aliases to the standard commands that we consider sane shortcuts that exist for backwards compatibility with earlier versions of Docker.

~~~
docker <context> <command> <target/arguments>
~~~

The context is usually stuff like `container`, `image`, or `network`. the command is with regards to the context given.

#### Examples

some examples would be

###### list running containers

~~~shell
docker container ls
~~~

###### list all defined containers running or stopped

~~~shell
docker container ls -a
~~~

###### list local images

~~~shell
docker image ls
~~~

###### remove a unused local images

~~~shell
docker image rm <imageID>
~~~

###### start a new `bash` shell inside an running container

~~~shell
docker container -it <containerID> bash
~~~

###### list the hosts docker networks

~~~shell
docker network ls
~~~

### Online help

You can always get online help regarding the `docker` command.

~~~shell
docker help
~~~

The output lists the docker "contexts" in the "Management Commands:" section.  You can get more specific help within a specific "context"

~~~shell
docker <context> help
~~~

For instance to find information regarding the `container` context:

~~~shell
docker container help
~~~

#### Explore

You now know the basics of the `docker`.  Explore around, see what you can find.  You are not going to hurt it.

#### Clean up

See if you can figure out how to remove all the defined containers and remove the images in the local image store?

Cleaning up will help us not have conflicts later.

###

This concludes part 3 of Hands-on Exercise #2.
