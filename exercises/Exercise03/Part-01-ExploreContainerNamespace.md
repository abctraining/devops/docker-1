# Exercise #3 : Part 1
## Explore container namespace

### Intro

In this lab we are going to look briefly at how Docker handles the namespaces for each container.  This is a complex topic. The standard userland namespace tools are beyond the scope of this lab.  We will be paying attention to the values of the `pid` namespace IDs linked to from `/proc/<pid>/ns/pid`.

#### PID namespace

It would be helpfull to have multiple shells open an the same time to your lab instance as we will be looking at commands bot inside containers and outside container directly on the host.

First let us get the ID number of the global PID namespace that our shell is running in.  The `$$` is a shell variable that expands to the PID number your current process or shell.  So on the host (not inside a container) run this command to see the ID of your global `pid` namespace.  Take note of it.

###### host

~~~shell
sudo ls -l /proc/$$/ns/pid
~~~

Now we should be able to run the same command to get the ID of the shell running inside of a container.  So make sure you are inside a container and run the same command

###### 1st container

~~~shell
ls -l /proc/$$/ns/pid
~~~

Now to demonstrate the point open an additional ssh connection to your lab instance start an additional container and run the same command.

###### 2nd container

~~~shell
ls -l /proc/$$/ns/pid
~~~

Now compare the three results.  What do you see?  Do they each have a unique `pid` namespace ID?

Just out of curiosity lets check what `pid` namespace PID 1 is on the host.

##### host

~~~shell
sudo ls -l /proc/1/ns/pid
~~~

Is that what you expected?  Why?

####

Now lets look at the actual processes and process trees.  On the host run this command.

###### host

~~~shell
ps -aux
~~~

Do you see all the processes?

Let's use a new `ubuntu` container to look at the processes that are running inside the container `pid` namespace.  Notice that we have the `--rm` option for the docker `run`.  That will cause the container to be remove when the container exits, we are treating it like cattle.

~~~shell
docker container run --rm -it ubuntu bash
~~~

We are going to use a tool called `pstree` to print out the processes in tree view.  Unfortunately the `ubuntu` container does not have it pre-installed so we will need to first install it on the container.

~~~shell
apt update && apt install -y psmisc
~~~

Now inside the container we can run `pstree` with the `-p` option to also include the PID numbers.

~~~shell
pstree -p
~~~

Now run that same `pstree` command this time on the host instead of inside a container.  Notice that difference?

#### compare process ID inside and outside the container

In this section we will compare what PID the same process has both inside and outside of a container.  To do this we need a long live process runing inside a container.  Go ahead an have `sleep` run in the background for an hour inside a container.

~~~shell
sleep 3600 &
~~~

Once that process is running look both on the host and on the container for the "sleep" PID by runnig this sequence of commands inside the container and on the host.

~~~shell
ps -aux | grep sleep | head -n 1
~~~

Notice the difference?  Take the PID from each perspective and check what PID name space that `sleep` process is in.

host
~~~bash
sudo ls -l /proc/<sleepPID>/ns/pid
~~~

container
~~~bash
ls -l /proc/<sleepPID>/ns/pid
~~~

Are the resulting `pid` namespace IDs the same?  They should be.

###

This concludes part 1 of Hands-on Exercise #3.  Continue on to Part 2 next.

[Part 2: Explore container cgroups](Part-02-ExploreContainerCgroups.md)
