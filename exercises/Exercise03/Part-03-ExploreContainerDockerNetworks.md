# Exercise #3 : Part 3
## Explore container Docker networks

### Intro

In this section let us look at how easy it is to create networks and attach networks to devices.  We will use `inspect` to get informaiton like what subnet or IP address the docker networks or containers are assigned.

### Create some networks

The first thing you need to do is create a few new network devices that use the `bridge` driver.

~~~shell
docker network create -d bridge network01
docker network create -d bridge network02
~~~

Now create a container connected to each of the new bridge networks.

### Attach some container to the new networks

~~~shell
docker container run -d -it --net=network01 --name net01 busybox
docker container run -d -it --net=network02 --name net02 busybox
~~~

#### Look at Subnets, IPs, and routes

Use `docker network inspect` to display the IP Address Management (IPAM) information assigned to the bridge networks.  Start with `network01`

~~~shell
docker network inspect --format '{{ .IPAM.Config }}' network01
~~~

You should be able to identify the subnet and the IP assigned to that bridge. Compare that to the `network02` bridge.

~~~shell
docker network inspect --format '{{ .IPAM.Config }}' network02
~~~

Notice that they are on different subnets. The Docker network bridges exist in the Docker hosts `global` `net` namespace.  We can verify that by looking at the hosts routing table.  Notice that there is a direct attach route for each subnet out each bridge device.

~~~shell
ip route show
~~~

#### the container IP addresses

Using `docker contaire inspect` we can display each containers ip address attached to each network.  Notice the different values that are queried based on what network the container is attached to.

~~~shell
docker container inspect --format '{{ .NetworkSettings.Networks.network01.IPAddress }}' net01
docker container inspect --format '{{ .NetworkSettings.Networks.network02.IPAddress }}' net02
~~~

Do you notice how each IP is assigned out of the corresponding bridges network?

#### Port Forwarding

Let see what Docker does to get traffic from outside the host into a running container by "publishing" ports from a container.  Create a new container running the `nginx` webserver "published" to port 8010 on the docker host.  On the host TCP port `8010` will be forwarded to port 80 within the container connected to the default "bridge".

~~~shell
docker container run -d -it -p 8010:80 --name web01 nginx:stable-alpine
~~~

Let us run a different web server, this time `Apache` (httpd) "published" on TCP port `8020` and connected to `network01`

~~~shell
docker container run -d -it --net=network01 -p 8020:80 --name web02 httpd:alpine
~~~

Create a third web server running in a container.  This time using a webserver called `caddy` "published" on TCP pourt `8030`, connected to `network02`.

~~~shell
docker container run -d -it --net=network02 -p 8030:80 --name web03 caddy:alpine
~~~

Like before let us verify the IPs assigned to each webserver.

~~~shell
docker container inspect --format '{{ .NetworkSettings.Networks.bridge.IPAddress }}' web01
docker container inspect --format '{{ .NetworkSettings.Networks.network01.IPAddress }}' web02
docker container inspect --format '{{ .NetworkSettings.Networks.network02.IPAddress }}' web03
~~~

The interesting part is how the "published" traffic makes it to the container.  IPTables DNAT rules are created for each "published" port.  We can query for the DNAT rules with this command.

~~~shell
sudo iptables -L -t nat -n | grep ^DNAT
~~~

Using your web browser see if you can connect to each web service running by your "hostname" or "IP" combined with the "published" port.

###### nginx

http://lab42.agilebrainslabs.com:8010

###### apache

http://lab42.agilebrainslabs.com:8020

###### caddy

http://lab42.agilebrainslabs.com:8030

### Conclusion

Docker Container can have multiple network connections.  See if you can create a container that has an interface connected to both `network01` and `network02`.  You should be able to see both interfaces with `docker container inspect` within the `NetworkSettings` section.

#### Clean-up

We have created a number of resources within docker during this exercise.  Let us clean up the containers and networks. First verify all the defined containers.

~~~shell
docker container ls -a
~~~

We could stop these containers one-by-one.  But why do it that way when you can accomplish the same with o single set of commands. grep, awk, & xargs are your friends.

~~~shell
docker container ls | awk '{print $1}' | grep -v CONTAINER | xargs docker stop
~~~

We should now see all the containes in a "stopped" state.

~~~shell
docker container ls -a
~~~

The is also a simple command to remove all stopped containers.  Be careful with this command.  Remember we treat our resources as "cattle" as much as possible and not pets.

~~~shell
docker container prune
~~~

Veify that the containers are gone.

~~~shell
docker container ls -a
~~~

We did create some extra networks.  We can see them with this command.

~~~shell
docker network ls
~~~

In a similare way to containers the `prune` command removes all none defalut unused networks.

~~~
docker network prune
~~~

### Conclusion

This concludes part 3 of Hands-on Exercise #3.
