# Exercise #3 : Part 2
## Explore container cgroups

### Intro

In this section we will take a look at how to limit resources that a container has access to.  Under the hood Docker using linux cgroups to apply limits to the processes in the containers.  Like namespaces, cgroups can be complex but Docker makes it easy.  Docker creates a cgroup for every resource type for each container identified by the containers ID.  In this exercise we will read the settings created by docker and compare the differences as we set different limits.

Before we get started we need to understand how `inspect` works.  First start a small container named "placebo" based on the `busybox` image and have it run in the background, detached (`-d`).

~~~shell
docker run -d -it --name placebo busybox
~~~

Now we can use `docker container inspect` to display metadata associated with the new container.

~~~shell
docker container inspect placebo
~~~

All the information is prsented by default stuctured as JSON.  You can look for one specific set of data within that JSON by using the `format` option with `describe`.  For instance to display the full container Id you can run this.

~~~shell
docker container inspect --format '{{ .Id }}' placebo
~~~

With this knowledge we can programmatic find the info we need in the cgroups for this exercise.

### CPUs


Create a container named `cpu50` and limit it to 50% of the cpu cycles with the `--cpus` option

~~~shell
docker run -d -it --cpus=".5" --name cpu50 busybox
~~~

Create an additional container named `cpu75` this time limited to 75%.

~~~shell
docker run -d -it --cpus=".75" --name cpu75 busybox
~~~

Now let's look at what values exist in the `cpu.cfs_quota_us` setting for each container.  The `placebo` container does not have a quota set so it will return and error when we query for it.

##### cpu50

~~~shell
cat /sys/fs/cgroup/cpu/docker/`docker container inspect --format '{{ .Id }}' cpu50`/cpu.cfs_quota_us
~~~

##### cpu75

~~~shell
cat /sys/fs/cgroup/cpu/docker/`docker container inspect --format '{{ .Id }}' cpu75`/cpu.cfs_quota_us
~~~

##### placebo

~~~shell
cat /sys/fs/cgroup/cpu/docker/`docker container inspect --format '{{ .Id }}' placebo`/cpu.cfs_quota_us
~~~

Compare the values returned.  Do you notice the differences?  Do you understand where the values are stored based on the container Id?

### Memory

Now let us look how memory is limited with Docker and cgroups.  We will create two more containers, `mem16` & `mem32` limited to 16MB and 32MB of memory.

~~~shell
docker run -d -it --memory 16m --name mem16 busybox
docker run -d -it --memory 32m --name mem32 busybox
~~~

Query the `memory.limit_in_bytes` value for the `mem16` container.

~~~shell
cat /sys/fs/cgroup/memory/docker/`docker container inspect --format '{{ .Id }}' mem16`/memory.limit_in_bytes
~~~

Now do the same for the `mem32` container

~~~shell
cat /sys/fs/cgroup/memory/docker/`docker container inspect --format '{{ .Id }}' mem32`/memory.limit_in_bytes
~~~

Again do you see the difference?  Hopefully by this time we have a bit of understanding how cgroups are managed by Docker.

###

This concludes part 2 of Hands-on Exercise #3.  Continue on to Part 3 next.

[Part 3: Explore container Docker networks](Part-03-ExploreContainerDockerNetworks.md)
