# Docker 1: Essentials
# Hands-on Exercise #3

### Objective

Look at how Docker uses Namespaces and cgroups in addition to how Docker networking is managed.

### Parts

[Part 1: Explore container namespaces](Part-01-ExploreContainerNamespace.md)

[Part 2: Explore container cgroups](Part-02-ExploreContainerCgroups.md)

[Part 3: Explore container Docker networks](Part-03-ExploreContainerDockerNetworks.md)
