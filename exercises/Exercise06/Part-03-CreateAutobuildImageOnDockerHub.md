# Exercise #6 : Part 1
## Create auto-bulid image on Docker Hub

### Intro

In this section we will return to Docker Hub and create an images that is built automatically when a new commit is made on the git repo that contains the `Dockerfile`.  As before during this exercise you will be much on your own to use the skills we have already learned to accomplish this task.

### Github / Bitbucket

You will need a Github account or a Bitbucket account to complet this exercise.  In addition to your Docker Hub account we used in part-01 of this exercise.

These accounts can be created at:

[Github](https://github.com) or [Bitbucket](https://bitbucket.org)

You only need an account on one of those providers you do not need both.  For the rest of this lab I will assume you are using Github.  If you choose bitbucket make adjustments as needed.

#####Create a git repo with a Dockerfile

For this exercise you will need to have a git repository on Github that has a Dockerfile in the repository in addition to any other files needed to build the docker image.

Creating this image is a little beyond the scope for this exercise but if git repos are something new to you, there are good instruction on your Github account.

Once you have a repo created add your Docker file in addition to other files needed to that git repo.

### Docker Hub

You should have a Docker Hub account from earlier.  On that account you can setup an auto-build image by linking a Docker image Registry to a `Dockerfile` provided on your git repo on  Github.

#### Account Settings -> Linked Accounts

You first need to connect your Docker Hub account with your Github account.  This is accomplished on the "Account Settings" -> "Linked Accounts" page.

#### Repositories Create Repository

Once you have the accounts linked you can "Create" a new repository.

##### Optional Build settings -> Connected

While creating the new repository their is a link for "Optional Build settings" where you can click on the "Connected" icon for Github.

Now you should be able to select your git repo that contains your `Dockerfile`.

While you are here expand "BUILD RULES" and notice the `Dockerfile` location. Make sure that is correct relative to your `Dockerfile` location in your git repo.

You should now be able to finish creating this Docker auto-build image repo and "trigger" a new build.  Now when ever the git repo changes.  Github will automatically build the image.

#### Bonus: Tag by branch

As a bonus more complex task see if you can have Docker Hub tag the images with a tag called "testing" when the Git branch is also called "testing".  You will also need to create a "testing" branch in your git repo for this exercise. Send an screenshot of the settings to the instructor.


### Conclusion

This concludes part 3 of Hands-on Exercise #6.
