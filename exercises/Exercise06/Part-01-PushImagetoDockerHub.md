# Exercise #6 : Part 1
## Push images to Docker Hub

### Intro

As we enter into this last exercise the format will be that this document will give you task and guidance where needed.  Your experince to this point should allow you to complete these tasks.

In this exercise we will use a docker hub account to push images to the public registry found at Docker Hub.

### Pull sources

On last time just in case. ;)

~~~shell
cd ~/content/docker-1/
git pull
~~~

#### Create Docker Hub account

You will need to have a Docker Hub account to complete these tasks.  If you do not already have one you can create the free account by following this link.

[hub.docker.com](https://hub.docker.com)

#### Login to Docker Hub

Once you have your Docker hub account you can login to that count from your Docker client on your lab instance.

~~~shell
docker login
~~~

#### Create an image

By this time you have created a number of docker images go ahead and create an additional image to use for this exercise however you choose.

#### Tag the image

Befor you can push an image to Docker Hub it needs to have a `tag` that is within your "usernames" namespace. Each image that are not "Official" images on Docker Hub are prefixed with a "namespace" that corresponds your username or an organization on Docker Hub that your account has permission to "push" to.

So for instance, my username on Docker Hub is `thejordanclark`. So if I had an local image called local/wordpress:latest that I wanted to push to Docker Hub with a similar name, I would need to `tag` the `local/wordpress:latest` image as `thejordanclark/wordpress:latest` before it is able to be pushed to Docker Hub.

~~~shell
docker image tag local/wordpress:latest thejordanclark/wordpress:latest
~~~

With that understanding you should be able to now tag that image that you created with a name that you want it to be on Docker Hub.  Verify that the image contains your new tag with `docker image ls`.

#### Push the image

Now should be able to push your image to Docker Hub with a command similar to this.

~~~shell
docker image push thejordanclark/wordpress:latest
~~~

If all goes well you will see the layers of you image pushed to Docker Hub.  Verify that the image shows up as a "Repository" on your Docker Hub account.  Once it is there you can use that image on any machine by using the name it has on Docker Hub (what you tagged it as)

For example

~~~shell
docker pull thejordanclark/wordpress
~~~

### Conclusion

This concludes part 1 of Hands-on Exercise #6.  Continue on to Part 2 next.

[Part 2: Create an image that drops privileges](Part-02-CreateImageThatDropsPrivileges.md)
