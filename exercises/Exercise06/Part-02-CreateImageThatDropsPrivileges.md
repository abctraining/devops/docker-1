# Exercise #6 : Part 2
## Create an image that drops privileges

### Intro

In this section you will setup a Docker image that drops runs the service as an unprivileged user.

#### USER

The ability to run commands as non-root users inside a container can be accompleshed a number of ways but one of the most straight forward way is by using the "USER" command in your Dockerfile.  The "USER" command changes the user that an further steps within the Dockerfile are executed as to the user provided.

So if there is a line in the Dockerfile like this.

~~~
USER john
~~~

Any following RUN, ENTRYPOINT, CMD steps would use the `john` user instead of the `root` user.

#### Put it together

For this exercise I have provided a `Dockerfile` an `index.html` and a `nginx.conf` file which are all the artifacts need to build an image that runs nginx as the `www` user instead of the `root` user.

Your task is to build that image and verify that the container built from that image has no processes runnning as `root`.  Remember you can print all the processes running in the contaner like this.

~~~shell
docker container exec ps
~~~

Also be aware that only root can open sockets on the network that are less then 1024. The Dockerfile provided listens on TCP port 8000 because it has to be a port number above 1025. See if you can get your container to "publish" to the host TCP Port 80 pointing to TCP port 8000 in the container.  Once that is complete you should be able to connect to this service on the standard http port.

http://lab42.agailbrainslabs.com

### Conclusion

This concludes part 2 of Hands-on Exercise #6.  Continue on to Part 3 next.

[Part 3: Create Auto-build image on Docker Hub](Part-03-CreateAutobuildImageOnDockerHub.md)
