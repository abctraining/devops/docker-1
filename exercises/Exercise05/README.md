# Docker 1: Essentials
# Hands-on Exercise #5

### Objective

### Parts

[Part 1: Manage container life-cycle](Part-01-ManageContainerLifeCycle.md)

[Part 2: Connect to containers](Part-02-ConnectToContainers.md)

[Part 3: Use Docker volumes](Part-03-UseDockerVolumes.md)
