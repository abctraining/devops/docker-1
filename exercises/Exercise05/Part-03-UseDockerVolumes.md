# Exercise #5 : Part 3
## Use Docker volumes

### Intro

In this section you will explore how to connect external volumes into containers.

### Docker bind mount volume

First things, first, you will need a location to mount a bind mount type volume into a container.

~~~shell
mkdir -p ~/volumes/html
~~~

### Add content to `~/volumes/html/index.html`

Go ahead and put a simple webpage into the `~/volumes/html/` directory.

~~~shell
cp ~/content/docker-1/src/Exercise05/html/index.html ~/volumes/html/
~~~

With your editor of choice modify the content of `~/volumes/html/index.html`.  As before replace the `NAME` part with your name to demonstrate that you are connected to the correct host.

##### Web container

Go ahead and start a container that has the `~/volumes/html/` (`/home/student/volumes/html`) bind mounted as a volume in the container.  Notice the `-v`, we can even set that mount to read-only (ro).

~~~shell
docker container run -d -it -p 80:80 -v /home/student/volumes/html:/usr/share/nginx/html:ro --name web01 nginx:stable-alpine
~~~

Verify that you can load the webpage. (don't forget to change the hostname)

http://lab42.agilebrainslabs.com

You can also find the bind mount in the containers mount table (mnt namespace)

~~~shell
docker exec web01 mount | grep \/usr\/share\/nginx\/html
~~~

##### More containers, same mount

Start two more containers using completely different web servers listing listening on differet ports connected to the same bind mount to searve the `index.html` page.

###### httpd

~~~shell
docker container run -d -it -p 4020:80 -v /home/student/volumes/html:/usr/local/apache2/htdocs/:ro --name web02 httpd:alpine
~~~

###### caddy

~~~shell
docker container run -d -it -p 4030:80 -v /home/student/volumes/html:/usr/share/caddy:ro --name web03 caddy:alpine
~~~

On your browser verify both new containers now on TCP port `4020` and TCP port `4030`.  They all should return the same page.

http://lab42.agilebrainslabs.com:4020

http://lab42.agilebrainslabs.com:4030

### Docker volume

Docker also has the concept of managing volumes internally to Docker.  These Docker volumes do not require us to create external spaces to host volumes like bind mounts require.  So we do not even have to create a directory.

I this section we are going to look at an entire web application in 'jenkins' to demonstrate the volumes.  Notice the different representation of the source of the volume `-v` compared to how it was referenced before.  This time with a Docker Volume we provide a volume name not a volume path. We also are going to start this initial `jenkins` container in the forground.

~~~shell
docker container run -p 8080:8080 -p 50000:50000 -v JenkinsData:/var/jenkins_home --name jenkins jenkins/jenkins:lts
~~~

watch the output of the container.  After a bit of time to initialize you will see a section that looks like this. Take note of it as it contains your initial generated admin password.

~~~
*************************************************************
*************************************************************
*************************************************************

Jenkins initial setup is required. An admin user has been created and a password generated.
Please use the following password to proceed to installation:

f43f6b8553e14e82a59007137b317ed3

This may also be found at: /var/jenkins_home/secrets/initialAdminPassword

*************************************************************
*************************************************************
*************************************************************
~~~

Connect to your jenkins instance on TCP port 8080.

http://lab42.agilebrainslabs.com:8080

Go ahead and finish the installation and create a new pipeline in Jenkins to reprsent changes that need to be preserved. When you are finished hit `Ctrl+C` to stop the container.

We see the container exists.

~~~shell
docker container ls -a
~~~

And there should now be a `JenkinsData` volume managed by Docker.

~~~shell
docker volume ls
~~~

Remove the `jenkins` container, remember it is like `cattle`.

~~~shell
docker container rm jenkins
~~~

verify the container is removed.

~~~shell
docker container ls -a
~~~

Fortunately Docker has presevered the `JenkinsData` volume.

~~~shell
docker volume ls
~~~

Thank you Docker for preserving the data.

#### Create a new Jenkins container

Create a new container with the same command as before.

~~~shell
docker container run -d -p 8080:8080 -p 50000:50000 -v JenkinsData:/var/jenkins_home --name jenkins jenkins/jenkins:lts
~~~

Now connect back to your Jenkins instance and verify that the state was preserved between instances of the container.

### Clean-up

When you are finish with the Exercise and your exploration, go ahead and clean up all the containers and volumes we are done with them.

### Conclusion

This concludes part 3 of Hands-on Exercise #5.
