# Exercise #5 : Part 1
## Manage container life-cycle

### Intro

In this section we will dive in a deeper to the management of containers and state.  We have been using many of these commands already but this will give us another look possibly from different angles.

### Pull sources

It is always a good idea to verify that our local git repo is the most current state.  We can change to the the repo directory and use `git pull` to make sure we have a the newest version of the repo.

~~~shell
cd ~/content/docker-1/
git pull
~~~

### search / pull / create / start / stop / rm

Let us walk through each step that happens through a containers life-cycle.  We do not always use every one of these commands because they are implied by other commands.  For instance if the `mongo` image is not in the hosts image store `docker container run -d mongo` implies:

`docker image pull mongo`
`docker container create mongo`
`docker container start <containerID>`

all in the one `run` command.  We are going to walk through each step to search for, pull, create, start, stop, & remove a container and image.

##### Search

The first step is to locate the image to use.  There are many ways to find images but since we are using the docker client we will use the `search` feature included within the docker client.  In our example here we will use `memcached` as a service to install.

~~~shell
docker search --filter is-official=true --no-trunc memcached
~~~

This returns a the official memcached image.  Try running the same command without the `--filter is-official=true` to see a list of images that match the search.

##### Pull

Now that you have the image you would like to use, that image needs to be pulled into the hosts image store.   To do that you can use `docker image pull`.  If you already have the image being pulled in the local image store, `docker image pull` will make sure your local stored image is the most recent version compared to what is in the registry.  We will use the "alpine" version of the `memcached` image.

~~~shell
docker image pull memcached:alpine
~~~

You can verify the exsting of the image in the local image store by having docker list the `memcached:alpine` image.

~~~shell
docker image ls memcached:alpine
~~~

##### Create

Since the `memcached:alpine` image is now local and up-to-date, we have everything we need to create a new container based on that image.  Docker `create` defines a new image but does not start the process associated with that image.

~~~shell
docker container create --name memcached memcached:alpine
~~~

The new container should now be created.  It is stopped so we can use the `-a` flag to see "all" containers on the system as we verify that the container exists.

~~~shell
docker container ls -a
~~~

Notice the "Status" of the memcached container.  Remember the container/process has yet to start.

##### Start

It is now time for you to start up the newly created container with the `start` command.

~~~shell
docker container start memcached
~~~

As before you can list all the containers on the host.

~~~shell
docker container ls -a
~~~

Now check out the "Status" of the memcached container.  You now have memcached deployed.  Nice.

##### Stop

Moving on in the container life-cycle it is now time to stop the running container.  For that, you guessed it, you use `docker container stop`

~~~shell
docker container stop memcached
~~~

Double check to make sure it really stopped by listing all containers again.

~~~shell
docker container ls -a
~~~

Did the "Status" of the memcached container change?  How long ago did the status of that container change?

##### Remove container

The container is stopped now let us remove the container from the host.

~~~shell
docker container rm memcached
~~~

Verify that the container no longer exists.

~~~shell
docker container ls -a
~~~

##### Remove image

The `memcached` container is now removed but `docker container rm` does not remove the image from the system.  You can demonstrate that by listing the image again.

~~~shell
docker image ls memcached:alpine
~~~

Go ahead and finish cleaning up by removing the `memcached:alpine` image from the host's image store.

~~~shell
docker image rm memcached:alpine -f
~~~

The image can only be removed if the are no defined (running or stopped) containers that are using / depending on that image.  Verify that the image has been removed.

~~~shell
docker image ls memcached:alpine
~~~

You have now manually worked through all the major steps with regards to Docker container life-cycles.

# On-Your-Own

Now using just the Docker CLI see if you can search for a Minecraft server image that has collected more then 500 stars.  Once you find an image.  Come up with a list of all the steps needed to search, deploy, remove / cleanup this Minecraft server using the image you found.  One of the steps needs to be a `pull` and you are not allowed to us `run`.  Once you have that list email that list to the instructor in addition to a screenshot to demonstrate that the container is running.

__Note:__ the image in question requires an environment variable set to indicate that you have accepted the EULA before it is allowed to start.  That environment variable can be set at `create` time by adding this option `-e EULA=TRUE`.

### Conclusion

This concludes part 1 of Hands-on Exercise #5.  Continue on to Part 2 next.

[Part 2: Connect to containers](Part-02-ConnectToContainers.md)
