# Exercise #5 : Part 2
## Connect to containers

### Intro

In this section we will work through different ways that you can interact with a running container or trouble shoot a container that needs some help.

#### Attach / Detach

If you recall all containers have a PID 1 process.  That process has a standard in / standard out interface.  If applicable you can attach to that interface.  That PID 1 process can be attached to to interact with it.  The simplest form of that would be viewing the output of standard out.  Go ahead and create and start a new image without the `-d` option to leave your shell attached to it.  Tomcat is noisy process go ahead and use that as your image.

~~~shell
docker run --rm -it --name tomcat tomcat:alpine
~~~

The `--rm` is just there to tell Docker to "Remove" the container when the process exits.  Yeah, automaticly removing your "cattle" :)

Back to 'tomecat', you will notice that there are a number of tomcat logs dumped to the standard out interface.  You should see something like "Starting Servlet Engine: Apache Tomcat" that indicates that 'tomcat' hast started.  But yet we are attached to the container and not dropped back onto the host's shell.  Fortunately there is a keystroke that detaches you from the container but leaves the container running.

`Ctrl+p` + `Ctrl+q`

when you are attached to a process, if you happened to hit `Ctrl+c` you would send a SIGTERM to the process and the container would stop.  Go ahead disconnect from the container but leave it running.

Once disconnected you can always reconnect to the container with the `attach` command.

~~~shell
docker container attach tomcat
~~~

Now this tomcat container's stardard interface is just shows logs it is not interactive like a shell.  So it will not do much with your input that you send it by type while attached.  Logs are only dumped every couple minutes so you may have to wait a bit to see some logs on PID 1's standard out.

When you are done waiting for more logs go ahead and `detach` from the process again without stopping the container, `Ctrl+p` + `Ctrl+q`.

#### Execute a process on a container

Sometimes you need to run an additional command inside a running container.  You can accomplish that by utilizing the `exec` command which executes a new process inside a docker container run the following couple commands to see where the WORKDIR is with `pwd` and what files exist inside that WORKDIR all inside the container.

~~~shell
docker exec tomcat pwd
~~~

~~~shell
docker exec tomcat ls -l
~~~

You also can do more then just read information.  If the process is interactive, like a shell, you can interact with it but you need to add `-i` (interactive) and `-t` (tty) options to `exec`.

~~~shell
docker exec -it tomcat bash
~~~

This will present you with an interactive shell running inside the container.  Notice the shell prompt has changed.  You also can look at the processes running inside the container.

~~~shell
ps
~~~

When you `exit` this shell the `bash` process stops but the container continues to run.

~~~shell
exit
~~~

You should now be back on the host system.  Notice again the change in the prompt.

#### Gather logs

You can dump the logs from the container to the with the `logs` command.

~~~shell
docker logs tomcat
~~~

That will dump the entire buffer holding the container logs.  Anouther option would be to conect to the logs but "follow" them with the `-f` option.

~~~shell
docker logs tomcat -f
~~~

This will dump the most recient logs but leave you attached to the logging interface that will read logs from the containers stardard out.

When you get tired of watching the logs feel free to disconnect with `Ctrl+c`

Now stop the container and let it clean-up after it's self, remember the `--rm`.

~~~shell
docker stop tomcat
~~~

### Conclusion

This concludes part 2 of Hands-on Exercise #5.  Continue on to Part 3 next.

[Part 3: Use Docker volumes](Part-03-UseDockerVolumes.md)
