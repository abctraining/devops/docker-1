# Docker 1: Essentials
# Hands-on Exercise #4

### Objective

Learn the basics of `Dockerfiles` and use those `Dockerfiles` to build custom Docker images.  In addition to how to explore and manage the Docker Images.

### Parts

[Part 1: Build a basic docker image](Part-01-BuildBasicDockerImage.md)

[Part 2: Modify a Dockerfile](Part-02-ModifyADockerfile.md)

[Part 3: Explore Docker Image Storage](Part-03-ExploreDockerImageStorage.md)
