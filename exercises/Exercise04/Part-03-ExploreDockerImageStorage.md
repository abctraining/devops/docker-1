# Exercise #4 : Part 3
## Explore Docker image storage

### Intro

In this section we will look at how the images are stored in the hosts image store.

### Location

The default location for the image store is in the `/var/lib/docker/images` directory on disk.  If you look around there you will discover a number of different datasets that represent each unique lay that makes up thee docker images.

### docker interface

We have used the `docker` interface a number of times already with `docker image` context. For instance to view the images in the local image store you have run.

~~~shell
docker image ls
~~~
More recently you have "built" new images based on `Dockerfiles` with `docker build`. Another handy option with images which can help get an idea how an image was created showing each layer is the `docker image history` command.

~~~shell
docker image history local/nginx:v2
~~~

As with other resources you can use the `inspect` option to get the metadata associated with an image.

~~~shell
docker image inspect local/nginx:1.0
~~~

Notice the "GraphDriver" section.  In that section you can find the location on disk for each layer of an image.

One last image command that is useful is the ability to remove images from an images store.

~~~shell
docker image rm hello-world:latest
~~~

There are a few more image topics we will be covering later when we cover the shipping of images from one host to another.

Can you guess what commands relate to the Shipping of containers?

### Conclusion

This concludes part 3 of Hands-on Exercise #4.
