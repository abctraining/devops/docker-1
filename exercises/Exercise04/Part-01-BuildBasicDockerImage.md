# Exercise #4 : Part 1
## Build basic Docker image

### Intro

In this section you will be introduced to `Dockerfiles` and the `docker build` commands.  You will be using a some resources that are in the `src` directory of the repo.

### Pull sources

It is always a good idea to verify that our local git repo is the most current state.  We can change to the the repo directory and use `git pull` to make sure we have a the newest version of the repo.

~~~shell
cd ~/content/docker-1/
git pull
~~~

### Basic build

Now create a directory to work from as you build Docker images.

~~~shell
mkdir -p ~/container/web-v1/html
~~~

Change to the directory and copy the artifacts that are needed from the `src` location.

~~~shell
cd ~/container/web-v1
cp ~/content/docker-1/src/Exercise04/v1/Dockerfile .
cp ~/content/docker-1/src/Exercise04/v1/index.html html/
~~~

Modify index.html replace the NAME string with your name.

Now you should be able to build a new image name `local/web:v1`.

~~~shell
docker image build -t local/web:v1 .
~~~

After the build has complemented you should now see the new image in the hosts local image store.

~~~shell
docker image ls
~~~

Let us see if the image succeeds by creating on new container based on the newly created image.

~~~
docker container run -d -it -p 80:80 --name static1 local/web:v1
~~~

The container is "published" on TCP port 80 so we should be able to connect to it from a local browser. Don't forget to change the url to your hostname or IP.

http://lab42.agilebrainslabs.com

Before we move on 'stop' the newly created `static1` container to allow the next exercise to use TCP port 80.

~~~shell
docker container stop static1
~~~


### More complex docker file

That `Dockerfile` was very simple. Next you will create a bit more complex container be starting with a fresh `alpine:3.11` system, installing `lighttpd` and configuring it to start the process when the container is run.

Make some space to work on this new web-v2 `Dockerfile` and artifacts.

~~~shell
mkdir -p ~/container/web-v2/html
cd ~/container/web-v2
~~~

As before let us copy some artifacts, including the `Dockerfile` from the `src` directory for the labs.  

~~~shell
cp ~/content/docker-1/src/Exercise04/v2/Dockerfile .
cp ~/content/docker-1/src/Exercise04/v2/index.html html/
~~~

As Before modify the `html/index.html` file and replace `NAME` with your name.

Build the new image version of the `local/web` image.  This time with a different version tag `v2`.

~~~shell
docker image build -t local/web:v2 .
~~~

As before let us start a new container listening on TCP port 80.

~~~shell
docker container run -d -it -p 80:80 --name static2 local/web:v2
~~~

Verify that the webpage loads with your new docker image built from `alpine:3.11`.

### On your own now

First look through the `Dockerfile` located at `~/container/web-v2/Dockerfile`, make sure you understand what each step is doing.  Once you have a decent understanding of what is going on, see if you can create a `local/web:v3` image that serves this webpage on TCP port 22.  As always replace the `NAME` section.

~~~html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Alpine 3.11 and lighttpd</title>
</head>
<body>
    custom image v3<br>
    NAME, Your Server is online
</body>
</html>
~~~

Then deploy the new `local/web:v3` image on a container named `static3` listening on TCP port 81.  Once it is deployed verify that it works and go ahead and brag about it.  Share the link to your new sit with the class instructor.

### Conclusion

This concludes part 1 of Hands-on Exercise #4.  Continue on to Part 2 next.

[Part 2: Modify a Dockerfile](Part-02-ModifyADockerfile.md)
