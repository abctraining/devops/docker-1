# Exercise #4 : Part 2
## Modify a Dockerfile

### Intro

In this exercise you will be creating a Docker image on your own to server a website.  The docker image to create should meet these criteria:

1. Based on the public `ubuntu:18.04` image

2. Server the webpage listed below using `nginx`. (Replace `NAME` with your name)

~~~html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Ubuntu 18.04 and nginx</title>
</head>
<body>
    custom image v4<br>
    NAME, Your Server is online
</body>
</html>
~~~

Good luck, you can do this.

### Need a bit of help?

Since this is not about knowing all about `Ubuntu` and `nginx`, These hints are listed for your benifit if you need.

### Hints

##### Test from with-in a container

It may be helpful to test the commands from within a container as you are building your Dockerfile.

~~~shell
docker run --rm -it -p8080:80 ubuntu:18.04 bash
~~~

##### Packages needed

(you can use `apt install -y package` to install and don't forget to run `apt update`)

~~~
nginx
~~~

##### Web root

The default web root location for the Ubuntu `nginx` package is:

~~~
/var/www/html
~~~

##### Start `nginx`

the command to start `nginx` in the for ground mode is

~~~shell
/usr/sbin/nginx -g "daemon off;"
~~~

### Conclusion

This concludes part 2 of Hands-on Exercise #4.  Continue on to Part 3 next.

[Part 3: Explore Docker Image Storage](Part-03-ExploreDockerImageStorage.md)
