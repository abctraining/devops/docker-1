# Docker 1: Essentials
# Hands-on Exercise #1

### Objective

Verify Lab connectivity and download lab content.

### Parts

[Part 1: Connect to Labs](Part-01-ConnectToLabs.md)

[Part 2: Setting up a web server and test connectivity](Part-02-SetupWebServerAndTestConnectivity.md)

[Part 3: Download lab content](Part-03-DownloadLabContent.md)
