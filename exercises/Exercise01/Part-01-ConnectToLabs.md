# Exercise #1 : Part 1
## Connect To Labs

### Intro

The labs are provided in the form of Virtual Machines available through ssh.  The lab instances are based on Ubuntu 18.04 LTS.  Each instance is a basic machine with a `student` user with sudo privileges.  The provided student user is availible to login via ssh using a private key.  All labs are written with the expectation of completing using the provided 'student' account.

The class instructor will provide the private key needed in one of two forms.

1. `ABC-Student.pem` - For use from Mac and Linux machines using the integrated OpenSSH client.

2. `ABC-Student.ppk` - For use from Windows machines via the Putty ssh client.

The class instructor will also provide a IP address and/or hostname of the Lab instance for each student.  Following are instructions on how to connect to the lab instances separated into two sections first for "Mac and Linux" the second for Windows.

### Connect to lab instance from a Mac or Linux machines

Before starting verify that you have a copy of the `ABC-Student.pem` file on your machine.  We will assume that it is located in the `~/Downloads` folder, if it is located somewhere adjust the paths accordingly.  Also verify that you have the hostname or IP of the Linux Lab Instance.

The following commands are run from a terminal app on your machine.

##### restrict ownership of the ssh private key

OpenSSH requires that the ownership of the ssh private key not be readable by users other then the file owner. So let's make sure that the `ABC-Student.pem` file has restrictive enough privilages.

~~~shell
chmod 400 ~/Downloads/ABC-Student.pem
~~~

##### Connect to the remote Lab instance

Now that we have set the correct permissions for the private key. We can now connect to the remote Lab Instance.  We will use the `-i` option for the OpenSSH client to indicate which private key to use at login.  Replace `lab42.agilebrainslabs.com` with your provided hostname or IP.

~~~shell
ssh -i ~/Downloads/ABC-Student.pem student@lab42.agilebrainslabs.com
~~~

##### Save the hosts public key to `known_hosts`

The first time you connect you will be prompted to accept the hosts public key.  Type `yes` to save the hosts public key into your `.ssh/known_hosts` file.

##### Verify the Connection

You should now be presented with a shell on your own lab instance.

### Connect to lab instance from a windows machines

Before starting verify that you have a copy of the `ABC-Student.ppk` file on your machine.  We will assume that it is located in the `~\Downloads` folder, if it is located somewhere adjust the paths accordingly when adding the private key to your sessions "Auth" section of your Putty.  Also verify that you have the hostname or IP of the Linux Lab Instance.

#### Special note regarding downloading the PPK file

Many windows browsers do not know how to handle `.ppk` files, because of this you may be presented with the content of the file rather then the option to download the file.  Most browsers allow you to download content as a file by pressing `ctrl+s` or selecting `Save Page as..`, but not all browsers support the `Save Pages as..` option, most notable Microsoft Edge.  You do have the option to copy and paste the contents into `Notepad` and save it as `ABC-Student.ppk` as indicated in the following screenshots.

###### Paste content into Notepad

![Paste to Notepad](images/PPK-01.png "Paste to Notepad")

###### Save file as ABC-Student.ppk

![Save ABC-Student.ppk](images/PPK-02.png "Save ABC-Student.ppk")

#### Install Putty

Putty can be installed from [https://putty.org](https://putty.org "https://putty.org")

Please follow the instructions there to install Putty to your local machine if you do not already have putty installed.

#### Setup your ABC-Lab Session in putty


##### set username to `student`
After opening the Putty app, expand `Connection` and select `Data`. In this location we can set the `Auto-login username` to `student`.  If you skip this step you will be prompted what user to connect as at connection time.

![Set Auto-login username](images/Putty-01.png "Set Auto-login username")

##### Set session to use the `ABC-Student.ppk` private key

Navigate to `Connection` -> `SSH` -> `AUTH`.  In this section `Browse...` to locate your `ABC-Student.ppk` file in the `Private key file for authentication` field.

![Set Private Key](images/Putty-02.png "Set Private Key")

##### Store you session for future use and make initial connection

Now we can go back to the `Session` section at the top of the configuration page.  From there put the hostname or IP provided to you in the `Host Name (or IP address)` section.

Then add a name to save this session in within Putty in `Saved Sessions` field.  Then click the `Save` key to save the new session.  You should notice your session stored below in the list of sessions.

Finally, click the `Open` key to open your new session.

![Save and Connect to host](images/Putty-03.png "Save and Connect to host")

##### Save host public key to Putty

The first time you connect to the host you will be presented with the hosts public key.  Select `Yes` to save the public key to Putty and continue on with the connection.

![Save host public key](images/Putty-04.png "Save host public key")

##### Verify the Connection

You should now be presented with a shell on your own lab instance.

![Verify Connection](images/Putty-05.png "Verify Connection")

###

This concludes part 1 of Hands-on Exercise #1.  Continue on to Part 2 next.

[Part 2: Setting up a web server and test connectivity](Part-02-SetupWebServerAndTestConnectivity.md)
