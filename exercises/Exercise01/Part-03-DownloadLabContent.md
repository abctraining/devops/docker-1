# Exercise #1 : Part 3
## Download lab content

### Intro

In this section we will download the source files needed for some of the Hands-on Exercises.


###

Let us create a location to store the hands-on exercise source files.

~~~shell
mkdir -p ~/content
~~~

We can change our  working directory to this newly created location by changing our directory to it.

~~~shell
cd ~/content
~~~

In this new folder we are going to use the `git` client to pull the lab content locally by cloning the repo using the https protocal.

~~~shell
git clone https://gitlab.com/abctraining/devops/docker-1.git
~~~

### A scavenger hunt

Now it's time for a scavenger hunt.  The `src` files are now located on your lab instances file system.  See if you can locate the file associated with Hands-on Exercise #1.  Once you locate that file you can view the content by concatenating it with the `cat` command.

###

You now have the access and tools needed to complete upcoming hands-on exercises.  Over the next days we will get quite a bit of hands on experience with Docker.

This concludes part 3 of Hands-on Exercise #1.
